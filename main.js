const electron = require('electron');
const {app, BrowserWindow} = electron;

var mainWindow;

app.on('window-all-closed', () => {
  app.quit();
});

app.on('ready', () => {
  mainWindow = new BrowserWindow({width: 800, height: 600});

  mainWindow.openDevTools();

  mainWindow.loadURL('file://' + __dirname + '/index.html');
});
