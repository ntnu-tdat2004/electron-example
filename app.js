os = require('os');
fs = require('fs');
const {remote} = require('electron');
const {app, webContents, Menu, MenuItem} = remote;

var application_menu = [
  {
    label: 'Electron-example',
    submenu: [
      {
        label: 'Open DevTools',
        accelerator: 'CmdOrCtrl+T',
        click: () => { webContents.getFocusedWebContents().openDevTools(); }
      },
      {
        label: 'Close DevTools',
        accelerator: 'Shift+CmdOrCtrl+T',
        click: () => { webContents.getFocusedWebContents().closeDevTools(); }
      },
      {
        label: 'Quit',
        accelerator: 'Command+Q',
        click: () => { app.quit(); }
      }
    ]
  }
];
menu = Menu.buildFromTemplate(application_menu);
Menu.setApplicationMenu(menu);


document.getElementById('test_button').addEventListener('click', ()=>{
  console.log("test_button clicked")
});


setInterval(()=> {
  document.getElementById('freemem_span').innerHTML=os.freemem();
}, 100);


fs.readFile('README.md', 'utf8', (error, data) => {
  if(error) {
    console.log("Error: "+error)
    return;
  }
  document.getElementById('readme_span').innerHTML=data.replace(/\n/g, '<br>');
});
