# Electron example

## Prerequisites
Install `Node.js`, `npm` and `git`.

## Clone repository
```sh
git clone https://gitlab.com/ntnu-tdat2004/electron-example
cd electron-example
```

## Install dependencies
### Linux
```sh
sudo npm install -g electron
```

### MacOS
```sh
npm install -g electron
```

## Run
```sh
electron .
```
